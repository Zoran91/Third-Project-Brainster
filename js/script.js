$(window).scroll(function() {
	if ($(document).scrollTop() > 50) {
		$('nav').addClass('shrink');
		$('.a-black').css('color', 'black');
	}else {
		$('nav').removeClass('shrink');
		$('.a-black').css('color', '#fff');
	}


});

window.sr = ScrollReveal();
sr.reveal('.experience-left h1', {
	duration: 1500,
	origin: 'top'
});

sr.reveal('.numbers h1', {
	duration: 1500,
	origin: 'top'
});


sr.reveal('.experience-right h1', {
	duration: 1500,
	origin: 'top'
});

sr.reveal('.experience-right p', {
	duration: 1500,
	origin: 'top'
});

sr.reveal('.hire', {
	duration: 1500,
	origin: 'bottom'
});

sr.reveal('.join', {
	duration: 1500,
	origin: 'bottom'
});

$(document).ready (function() {
	$('#scroll-down').click( function(){
		$('#scroll-pic').animate({
			'bottom' : '-399px'
		});
	});

	$('#scroll-up').click( function(){
		$('#scroll-pic').animate({
			'bottom': '0px'
		});
	});


	$('#scroll-pic').scroll(function(){

	});

	$('.para-scroll').scroll(function(){
		if ($('.para-scroll').scrollTop() > 10) {
			$('p').addClass('.text-red');
		}
	});




	$('#empathetic').click(function() {
		$('.empathetic p').show('slow');
		$('.accountable p').hide('slow');
		$('.focused p').hide('slow');
	});

	$('#accountable').click(function() {
		$('.empathetic p').hide('slow');
		$('.accountable p').show('slow');
		$('.focused p').hide('slow');
	});

	$('#focused').click(function() {
		$('.empathetic p').hide('slow');
		$('.accountable p').hide('slow');
		$('.focused p').show('slow');
	});

	$('.color-red1').click(function() {
		$('#branding').show('slow');
		$('#filming').hide('slow');
		$('#coding').hide('slow');
	});

	$('.color-red2').click(function() {
		$('#branding').hide('slow');
		$('#filming').show('slow');
		$('#coding').hide('slow');
	});

	$('.color-red3').click(function() {
		$('#branding').hide('slow');
		$('#filming').hide('slow');
		$('#coding').show('slow');
	});



$('#myCarousel').bind('mousewheel', function(e){
        if(e.originalEvent.wheelDelta /120 > 0) {
            $(this).carousel('next');
        }
        else{
            $(this).carousel('prev');
        }
    });


/* SLIDE ON CLICK */ 

$('.carousel-linked-nav > li > a').click(function() {

    // slide to number -1 (account for zero indexing)
    $('#myCarousel').carousel(item - 1);

    // remove current active class
    $('.carousel-linked-nav .active').removeClass('active');

    // add active class to just clicked on item
    $(this).parent().addClass('active');

    // don't follow the link
    return false;
});



});








